<?php

namespace App\Service;

use Pimcore\Model\Listing\AbstractListing;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject\LatestNews\Listing as LatestNews;

class LatestNewsQueryBuilder extends AbstractListingQueryBuilder
{
    public function __construct(AbstractListing $listing, Request $request)
    {
        $this->listing = $listing;
        $this->request = $request;
    }
}
