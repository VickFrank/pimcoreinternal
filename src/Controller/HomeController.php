<?php

namespace App\Controller;

use Pimcore\Controller\FrontendController;
use Pimcore\Model\Asset;
use Pimcore\Model\DataObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class HomeController extends FrontendController
{
    /**
     * @Template
     * @param Request $request
     * @return Response
     */
    public function listingAction(Request $request)
    {
        $asset = Asset::getById(4);

        return $this->render('home/listing.html.twig', [
            'asset' => $asset,
        ]);
    }
}
