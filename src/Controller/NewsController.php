<?php

namespace App\Controller;

use Pimcore\Controller\FrontendController;
use Pimcore\Model\DataObject\LatestNews;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends FrontendController
{
    #[Route('/news/{slug}-{newsId}', name: 'latest_news_show', requirements: ["slug" => "[\w-]+", "newsId" => "\d+"])]
    public function showAction(Request $request, int $newsId): Response
    {
        $latestNews = LatestNews::getById($newsId);

        if (empty($latestNews)) {
            throw new \Exception('No blog post is found');
        }

        return $this->render(
            'news/show.html.twig', [
                'latestNews' => $latestNews
        ]);
    }

    public function listingAction(): Response
    {
        return $this->render('news/listing.html.twig');
    }
}
