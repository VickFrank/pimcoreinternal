<?php

namespace App\Controller;

use Pimcore\Model\DataObject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends \Pimcore\Controller\FrontendController
{
    /**
     * @Template()
     */
    public function listingAction(Request $request)
    {
        $objects = array();
        if ('object' === $request->get('type')) {
            $object = DataObject::getById((int) $request->get('id'));
            if ('folder' === $object->getType()) {
                return [
                    'objects' => $object->getChildren()
                ];
            }
        }

        return [ 'objects' => $objects ];
    }
}
