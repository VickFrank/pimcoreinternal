<?php

namespace App\Website\LinkGenerator;

use Pimcore\Model\DataObject\LatestNews;
use http\Exception\InvalidArgumentException;
use Pimcore\Model\DataObject\Concrete;
use Pimcore\Tool;
use Pimcore\Twig\Extension\Templating\PimcoreUrl;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class LatestNewsLinkGenerator implements \Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface
{

    public function __construct(
        private SluggerInterface $slugger,
        private PimcoreUrl $pimcoreUrl,
        private ContainerInterface $container
    )
    {
    }

    /**
	 * @inheritDoc
	 */
	public function generate(Concrete $object, array $params = []): string
	{
        if (!$object instanceof LatestNews) {
            throw new InvalidArgumentException('Given object is not a LatestNews');
        }

        $slug = $this->slugger->slug($object->getTitle());

        $link = $this->pimcoreUrl->__invoke(
            [
                'slug' => $slug,
                'newsId' => $object->getId()
            ],
            'latest_news_show',
            true
        );

        if (!str_contains($link, 'https://') && !str_contains($link, 'http://')) {
            $protocol = $this->container->getParameter('site_protocol');
            $link = Tool::getHostUrl($protocol).$link;
        }

        return $link;
	}
}
