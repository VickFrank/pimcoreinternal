<?php

namespace App\Document\Areabrick;

use Pimcore\Model\Document\Editable\Area\Info;
use Pimcore\Model\DataObject\LatestNews as News;
use Pimcore\Model\User;
use Pimcore\Tool\Session;

class LatestNews extends AbstractAreaBrick
{
    public function getName()
    {
       return 'LatestNews';
    }

    public function action(Info $info)
    {
        $latestNews = new News\Listing();

        $latestNews->setOrderKey('o_creationDate');
        $latestNews->setOrder('desc');
        $latestNews->setLimit(3);

        foreach ($latestNews as $news) {
            $news->author = User::getById((int) $news->getPostedBy());
            $news->generatedLink = $news
                ->getClass()
                ->getLinkGenerator()
                ->generate($news);

            $news->save();
        }

        $info->setParams([ 'latestNews' => $latestNews->load() ]);
    }
}
